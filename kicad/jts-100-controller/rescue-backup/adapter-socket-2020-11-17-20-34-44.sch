EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:connectors
LIBS:IC
LIBS:mech
LIBS:mkz
LIBS:passive
LIBS:semiconductor
LIBS:team14
LIBS:xell
LIBS:mpu-6050
LIBS:jts-100
LIBS:team14-jts-100-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5100 2100 1    60   BiDi ~ 0
SDA
Text HLabel 5600 2000 1    60   Input ~ 0
SCL
Text HLabel 4200 1250 0    60   Input ~ 0
3V3
Text HLabel 3800 3750 0    60   Input ~ 0
RightHeater+
Text HLabel 3800 4750 0    60   Input ~ 0
RightHeater-
Text HLabel 5100 5800 3    60   Input ~ 0
RightSense+
Text HLabel 6700 3750 2    60   Input ~ 0
LeftHeater+
Text HLabel 6700 4750 2    60   Input ~ 0
LeftHeater-
Text HLabel 5600 5800 3    60   Input ~ 0
LeftSense+
Text HLabel 5250 5800 3    60   Input ~ 0
Earth
Wire Wire Line
	5100 2100 5100 3450
Wire Wire Line
	5250 1750 5250 3450
Wire Wire Line
	5450 1250 5450 3450
Wire Wire Line
	5600 2000 5600 3450
$Comp
L D_TVS_x2_ACom_KKA D1001
U 1 1 5AA41DF4
P 6100 2650
F 0 "D1001" H 6150 2550 50  0000 C CNN
F 1 "SP3222" H 6100 2750 50  0000 C CNN
F 2 "standardSMD:SOD-883" H 6100 2650 50  0001 C CNN
F 3 "" H 6100 2650 50  0001 C CNN
F 4 "Value" H 6100 2650 60  0001 C CNN "Fieldname"
	1    6100 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 2350 5100 2350
Connection ~ 5100 2350
Wire Wire Line
	6100 2950 5600 2950
Connection ~ 5600 2950
Wire Wire Line
	5250 2100 6400 2100
Wire Wire Line
	6400 2100 6400 2650
Wire Wire Line
	6400 2650 6300 2650
Connection ~ 5250 2100
Text Notes 6250 2850 0    60   ~ 0
ESD-Protection
$Comp
L C C1001
U 1 1 5AA440AA
P 4450 1500
F 0 "C1001" H 4475 1600 50  0000 L CNN
F 1 "100n" H 4475 1400 50  0000 L CNN
F 2 "standardSMD:C1608m" H 4488 1350 50  0001 C CNN
F 3 "" H 4450 1500 50  0001 C CNN
	1    4450 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4200 1250 5450 1250
Wire Wire Line
	4450 1650 4450 1900
Connection ~ 4450 1750
Wire Wire Line
	4450 1350 4450 1250
Connection ~ 4450 1250
Wire Wire Line
	3800 3750 4600 3750
Wire Wire Line
	4600 4750 3800 4750
Wire Wire Line
	6100 3750 6700 3750
Wire Wire Line
	6700 4750 6100 4750
Wire Wire Line
	5600 5800 5600 5050
NoConn ~ 5450 5050
Wire Wire Line
	5250 5050 5250 5800
Wire Wire Line
	5100 5050 5100 5800
Text Notes 5150 4300 0    60   ~ 0
Front View
$Comp
L JTS-100-Cartridge-Jack-Top X1001
U 1 1 5AA66EFB
P 5350 4000
F 0 "X1001" H 4450 4800 60  0000 C CNN
F 1 "JTS-100-Cartridge-Jack-Top" H 5350 3900 60  0000 C CNN
F 2 "jts-100:Half-Cartridge-Jack" H 4725 3775 60  0001 C CNN
F 3 "" H 4725 3775 60  0001 C CNN
	1    5350 4000
	1    0    0    -1  
$EndComp
$Comp
L JTS-100-Cartridge-Jack-Bottom X1002
U 1 1 5AA67138
P 5350 4500
F 0 "X1002" H 4450 3700 60  0000 C CNN
F 1 "JTS-100-Cartridge-Jack-Bottom" H 5350 4600 60  0000 C CNN
F 2 "jts-100:Half-Cartridge-Jack" H 4725 4275 60  0001 C CNN
F 3 "" H 4725 4275 60  0001 C CNN
	1    5350 4500
	1    0    0    -1  
$EndComp
Text Notes 650  7600 0    100  ~ 0
Note: \nJBC uses Hirose Plug/Sockets:\nStation: HIROSE - RPC1-12RB-6P(73)\nHand Pice: RPC1-12P-6S(73)
$Comp
L GND #PWR052
U 1 1 5AB33A6E
P 4450 1900
F 0 "#PWR052" H 4450 1650 50  0001 C CNN
F 1 "GND" H 4450 1750 50  0000 C CNN
F 2 "" H 4450 1900 50  0001 C CNN
F 3 "" H 4450 1900 50  0001 C CNN
	1    4450 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 1750 5250 1750
Wire Wire Line
	5250 5200 7400 5200
Wire Wire Line
	7400 5200 7400 5450
Connection ~ 5250 5200
$Comp
L R R701
U 1 1 5AB370C0
P 7400 5600
F 0 "R701" V 7480 5600 50  0000 C CNN
F 1 "100k/250mW" V 7300 5600 50  0000 C CNN
F 2 "standardSMD:R3216m" V 7330 5600 50  0001 C CNN
F 3 "" H 7400 5600 50  0001 C CNN
	1    7400 5600
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR053
U 1 1 5AB37135
P 7400 5850
F 0 "#PWR053" H 7400 5600 50  0001 C CNN
F 1 "GND" H 7400 5700 50  0000 C CNN
F 2 "" H 7400 5850 50  0001 C CNN
F 3 "" H 7400 5850 50  0001 C CNN
	1    7400 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 5850 7400 5750
$EndSCHEMATC
