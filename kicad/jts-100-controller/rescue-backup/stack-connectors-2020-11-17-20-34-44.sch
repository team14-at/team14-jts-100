EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:connectors
LIBS:IC
LIBS:mech
LIBS:mkz
LIBS:passive
LIBS:semiconductor
LIBS:team14
LIBS:xell
LIBS:mpu-6050
LIBS:jts-100
LIBS:team14-jts-100-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_02x06_Odd_Even X903
U 1 1 5AB6A11F
P 5800 4850
F 0 "X903" H 5850 5150 50  0000 C CNN
F 1 "WE-61001221121" H 5850 4450 50  0000 C CNN
F 2 "we-board-to-board:610X1221121" H 5800 4850 50  0001 C CNN
F 3 "" H 5800 4850 50  0001 C CNN
	1    5800 4850
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x06_Odd_Even X902
U 1 1 5AB6A126
P 5800 2300
F 0 "X902" H 5850 2600 50  0000 C CNN
F 1 "WE-610312243021" H 5850 1900 50  0000 C CNN
F 2 "jts-100:we-610X12243021-mirrored" H 5800 2300 50  0001 C CNN
F 3 "" H 5800 2300 50  0001 C CNN
	1    5800 2300
	1    0    0    -1  
$EndComp
Text Notes 5500 5400 0    60   ~ 0
SMT-Pin Header
Text Notes 5500 2950 0    60   ~ 0
SMT Socket Header, \nBottom Entry
Text Notes 5200 5850 0    60   ~ 0
This Combination allows\na board distance of 9mm\nif the pins can penetrate the \ntop board some 10th of millimeters. 
Text Notes 5500 4350 0    60   ~ 0
Bottom Board, \ntop side
Text Notes 5550 1950 0    60   ~ 0
Top Board, \nbottom side
Wire Wire Line
	6100 2200 6400 2200
Text GLabel 6400 2200 2    50   BiDi ~ 0
SDA-top
Text GLabel 6400 2400 2    50   Output ~ 0
3V3-top
Wire Wire Line
	6400 2400 6100 2400
Text GLabel 6400 2300 2    50   Output ~ 0
SCL-top
Wire Wire Line
	6400 2300 6100 2300
Text GLabel 6400 2500 2    50   UnSpc ~ 0
GND-top
Wire Wire Line
	6400 2500 6100 2500
Wire Wire Line
	6100 4750 6400 4750
Text GLabel 6400 4750 2    50   BiDi ~ 0
SDA-bot
Text GLabel 6400 4950 2    50   Input ~ 0
3V3-bot
Wire Wire Line
	6400 4950 6100 4950
Text GLabel 6400 4850 2    50   Input ~ 0
SCL-bot
Wire Wire Line
	6400 4850 6100 4850
Text GLabel 6400 5050 2    50   UnSpc ~ 0
GND-bot
Wire Wire Line
	6400 5050 6100 5050
$EndSCHEMATC
