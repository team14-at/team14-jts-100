Changelog for JTS-100
=====================
by Team14
---------



# Development

* use bottom entry socket header for OLED display
* moved MPU-6050 to top print
* Replaced analog inputs with the LTC2444, which can sense negative voltages of up to -0.3V
  It has 24 bits, therefore no analog signal conditioning is needed. 
* Add SWD-Connector (2x5 1.27mm pin header for black magic probe)
* fixed USB-footprint
* defined push buttons WE-434331013822, and fixed Würth footprint




## TODO
* check configurations of the UniSolder sourcecode, to get an overview, of all the needed 
  input voltage ranges. 
* switchable pullup resistors for measuring resistive sensors
* choose voltage reference chip
* set stepper to 5V
* split power connector into 4 solderpads
* split signals into top/bottom board. 
* draw footprint for current sensor
* check for 5V tolerant IO for ADC/MCU
* place Push Buttons (cutout in PCB edge needed?)
* connect ADC
