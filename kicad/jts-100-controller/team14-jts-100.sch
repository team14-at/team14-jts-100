EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 9
Title "JTS-100"
Date "2018-02-26"
Rev "work in progress"
Comp "Team14 GesbR"
Comment1 "Open Source Hardware"
Comment2 "Creative Commons 4.0 (CC-BY)"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 8350 2800 2150 2250
U 5A907B12
F0 "Analog" 60
F1 "analog.sch" 60
F2 "3V3" I L 8350 4850 60 
F3 "VIN" I L 8350 4650 60 
F4 "EN-Right" I L 8350 3050 60 
F5 "EN-Left" I L 8350 2950 60 
$EndSheet
$Sheet
S 900  2800 2450 2150
U 5A907B15
F0 "Digital" 60
F1 "digital.sch" 60
F2 "D+" O R 3350 2950 60 
F3 "D-" O R 3350 3050 60 
F4 "VUSB" O R 3350 4800 60 
F5 "3V3" I R 3350 4700 60 
F6 "SCL" B R 3350 3250 60 
F7 "SDA" B R 3350 3350 60 
F8 "Button1" O R 3350 3550 60 
F9 "Button2" O R 3350 3650 60 
$EndSheet
$Sheet
S 4550 5700 2350 700 
U 5A907B18
F0 "Power Supply" 60
F1 "power-supply.sch" 60
F2 "VIN" O R 6900 5800 60 
F3 "VUSB" I L 4550 5800 60 
F4 "3.9V" O R 6900 6000 60 
F5 "3V3" O R 6900 6100 60 
F6 "PE" O R 6900 6300 60 
F7 "VoltageControll-PWM" I L 4550 5900 60 
$EndSheet
Wire Wire Line
	6900 5800 7750 5800
Wire Wire Line
	7750 5800 7750 4650
Wire Wire Line
	7750 4650 8350 4650
Wire Wire Line
	6900 6100 7150 6100
Wire Wire Line
	7850 6100 7850 4850
Wire Wire Line
	7850 4850 8350 4850
Wire Wire Line
	7150 6100 7150 5300
Wire Wire Line
	7150 5300 3900 5300
Wire Wire Line
	3900 5300 3900 4700
Wire Wire Line
	3350 4700 3900 4700
Connection ~ 7150 6100
Wire Wire Line
	3350 4800 3550 4800
Wire Wire Line
	3550 4800 3550 5800
Wire Wire Line
	3550 5800 4550 5800
Wire Wire Line
	3350 2950 4550 2950
Wire Wire Line
	4550 3050 3350 3050
Wire Wire Line
	3350 3550 4550 3550
Wire Wire Line
	3350 3650 4550 3650
Wire Wire Line
	3350 3250 4550 3250
Wire Wire Line
	4550 3350 3350 3350
Wire Wire Line
	6900 2950 8350 2950
Wire Wire Line
	6900 3050 8350 3050
Wire Wire Line
	6900 6300 10850 6300
Connection ~ 3900 4700
Text Notes 4650 950  0    80   ~ 16
JBC compatible soldering controller\nbased on the open source soldering iron TS-100\nwith dual power supply for up to 250W @ 48V
Text Notes 600  7600 0    80   ~ 0
Main Differences to the TS-100:\n* motion sensor: MPU-6050\n* variable supply up to 48V\n* proper N-channel MOSFETs\n* dual channel for tweezers\n* current sensor\n* exchangeable cartridge adapter
$Sheet
S 8350 1050 2150 1400
U 5AA3F65E
F0 "Adapter Socket" 60
F1 "adapter-socket.sch" 60
F2 "SDA" B L 8350 2000 60 
F3 "SCL" I L 8350 1900 60 
F4 "3V3" I L 8350 1800 60 
F5 "RightHeater+" I R 10500 1200 60 
F6 "RightHeater-" I R 10500 1400 60 
F7 "RightSense+" I R 10500 1300 60 
F8 "LeftHeater+" I R 10500 1600 60 
F9 "LeftHeater-" I R 10500 1800 60 
F10 "LeftSense+" I R 10500 1700 60 
F11 "Earth" I R 10500 2100 60 
$EndSheet
Text Notes 5300 2250 0    60   ~ 0
TODO: \nseparate into 2 PCBs\nCheck LM5060 connections\nreplace power connector with wire-pads\nadd SWD-connector for development. \n\nKiCad 5 Migration\n
$Sheet
S 4550 2800 2350 2150
U 5A907A91
F0 "Microcontroller" 60
F1 "mcu.sch" 60
F2 "USB_D+" I L 4550 2950 60 
F3 "USB_D-" I L 4550 3050 60 
F4 "SCL" B L 4550 3250 60 
F5 "SDA" B L 4550 3350 60 
F6 "Button1" I L 4550 3550 60 
F7 "Button2" I L 4550 3650 60 
F8 "3V3" I L 4550 4700 60 
F9 "EN-Rigth" O R 6900 3050 60 
F10 "EN-Left" O R 6900 2950 60 
F11 "MISO" I R 6900 3300 60 
F12 "MOSI" O R 6900 3400 60 
F13 "SCLK" O R 6900 3500 60 
F14 "~CS_ADC" O R 6900 3600 60 
F15 "Supply-PWM" O R 6900 4700 60 
$EndSheet
Wire Wire Line
	10850 6300 10850 2100
Wire Wire Line
	10850 2100 10500 2100
Wire Wire Line
	6900 4700 7100 4700
Wire Wire Line
	7100 4700 7100 5150
Wire Wire Line
	7100 5150 4300 5150
Wire Wire Line
	4300 5150 4300 5900
Wire Wire Line
	4300 5900 4550 5900
$Sheet
S 900  5450 2150 850 
U 5AB69DD0
F0 "Stack Connectors" 60
F1 "stack-connectors.sch" 60
$EndSheet
Wire Wire Line
	7150 6100 7850 6100
Wire Wire Line
	3900 4700 4550 4700
$EndSCHEMATC
