Changelog jts-100-cartridge-adapter-jbs-t245
============================================


# V18.0.1-dev
* zweiten Federkontakt (X49) angeschlossen. War aus Versehen nicht kontaktiert
* Symbol-Pins von der Steckverbinder in der Lib von "Input" auf "Passive" korrigiert
* Sollbruchstellen verbessert (optimal: 0.3mm Bohrung im Raster von 0.6mm)

## TODOs
* Federn werden aktuell ca. 0.5mm zusammengedrückt (von 2.0mm auf 1.5mm). 
  Empfohlene Arbeitshöhe (von Würth) ist 1.6 bis 1.7mm. 
  Bei 1.5mm werden sie bereits plastisch deformiert. 
  Sollte im Mech-Design korrigiert werden. 
* Unbedingt Leiterplattendicke 1.0mm bestellen!
* Pad-Numerierung verbessern ist etwas inkonsistent
* Dev-Platine designen und mitbestellen. 
* Überall Cu-Flächen zur Temperaturverteilung am Griffstück


# V18.0.0
## Bestellt bei MultiCB
* 1.6mm Dicke (falsch, soll = 1.0mm)
* HAL Bleifrei
* Keine Schablone

