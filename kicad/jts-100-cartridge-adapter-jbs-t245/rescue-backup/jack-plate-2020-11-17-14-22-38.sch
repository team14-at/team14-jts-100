EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:jts-100
LIBS:jts-100-cartridge-adapter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L JTS-100-Cartridge-Jack J2
U 1 1 5A9D5A06
P 8300 3550
F 0 "J2" H 7800 4000 60  0000 C CNN
F 1 "JTS-100-Cartridge-Jack" H 9900 3100 60  0000 C CNN
F 2 "jts-100:Cartridge-Jack" H 7675 3325 60  0001 C CNN
F 3 "" H 7675 3325 60  0001 C CNN
F 4 "Value" H 8300 3550 60  0001 C CNN "Fieldname"
	1    8300 3550
	1    0    0    -1  
$EndComp
$Comp
L JTS-100-Dual-Hex-connector X50
U 1 1 5A9D5D2D
P 5250 3600
F 0 "X50" H 5250 3600 60  0000 C CNN
F 1 "JTS-100-Dual-Hex-connector" H 6250 3200 60  0000 C CNN
F 2 "jts-100:Dual-Hex-Connector" H 5150 3300 60  0001 C CNN
F 3 "" H 5150 3300 60  0001 C CNN
	1    5250 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3300 7550 3300
Wire Wire Line
	7550 3300 7550 2350
Wire Wire Line
	7550 2350 4500 2350
Wire Wire Line
	4500 2350 4500 3500
Wire Wire Line
	4500 3500 4650 3500
Wire Wire Line
	4750 3300 4750 2400
Wire Wire Line
	4750 2400 8050 2400
Wire Wire Line
	8050 2400 8050 3000
Wire Wire Line
	8200 3000 8200 1550
Wire Wire Line
	8200 1550 5150 1550
Wire Wire Line
	5150 1550 5150 3000
Wire Wire Line
	5350 3000 5350 1600
Wire Wire Line
	5350 1600 8400 1600
Wire Wire Line
	8400 1600 8400 3000
Wire Wire Line
	9050 3300 9050 3300
Wire Wire Line
	8550 2050 5750 2050
Wire Wire Line
	5750 2050 5750 3300
Wire Wire Line
	9050 3800 9050 3800
Wire Wire Line
	9050 3800 9050 5150
Wire Wire Line
	9050 5150 6100 5150
Wire Wire Line
	6100 5150 6100 3700
Wire Wire Line
	6100 3500 5850 3500
Wire Wire Line
	6100 3700 5850 3700
Wire Wire Line
	7550 3800 7550 3800
Wire Wire Line
	4500 4900 8050 4900
Wire Wire Line
	4500 4900 4500 3900
Wire Wire Line
	4400 3700 4650 3700
Wire Wire Line
	4500 3900 4750 3900
Wire Wire Line
	5150 4200 5150 5500
Wire Wire Line
	5150 5500 8200 5500
Wire Wire Line
	8200 5500 8200 4100
Wire Wire Line
	8400 4100 8400 5450
Wire Wire Line
	8400 5450 5350 5450
Wire Wire Line
	5350 5450 5350 4200
Wire Wire Line
	5750 3900 5750 5050
Wire Wire Line
	5750 5050 8550 5050
Wire Wire Line
	8550 5050 8550 4100
Text Notes 8100 3550 0    60   ~ 0
Front View
Wire Wire Line
	8550 2050 8550 3000
Wire Wire Line
	6100 3500 6100 2100
Wire Wire Line
	6100 2100 9050 2100
Wire Wire Line
	9050 2100 9050 3300
Wire Wire Line
	8050 4900 8050 4100
Wire Wire Line
	7550 3800 7550 4800
Wire Wire Line
	7550 4800 4400 4800
Wire Wire Line
	4400 4800 4400 3700
Text Notes 5050 3800 0    60   ~ 0
Front View
Text Label 4500 3000 1    60   ~ 0
HR+
Text Label 4400 4450 1    60   ~ 0
HTR-
Text Label 6100 4450 1    60   ~ 0
HTL-
Text Label 6100 3000 1    60   ~ 0
HL+
$EndSCHEMATC
