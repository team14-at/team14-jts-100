EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L jts-100:JTS-100-Cartridge-Jack-Top X301
U 1 1 5AB23B0D
P 3900 2650
F 0 "X301" H 3400 3100 60  0000 C CNN
F 1 "JTS-100-Cartridge-Jack-Top" H 3900 2700 60  0000 C CNN
F 2 "jts-100:Half-Cartridge-Jack" H 3275 2425 60  0001 C CNN
F 3 "" H 3275 2425 60  0001 C CNN
	1    3900 2650
	1    0    0    -1  
$EndComp
$Comp
L jts-100:JTS-100-Cartridge-Jack-Bottom X302
U 1 1 5AB23B64
P 3900 2850
F 0 "X302" H 3100 2450 60  0000 C CNN
F 1 "JTS-100-Cartridge-Jack-Bottom" H 3950 2800 60  0000 C CNN
F 2 "jts-100:Half-Cartridge-Jack" H 3275 2625 60  0001 C CNN
F 3 "" H 3275 2625 60  0001 C CNN
	1    3900 2850
	1    0    0    -1  
$EndComp
$Comp
L jts-100-cartridge-adapter-rescue:Conn_01x06 J301
U 1 1 5AB23BF7
P 7050 1400
AR Path="/5AB23BF7" Ref="J301"  Part="1" 
AR Path="/5AB237B6/5AB23BF7" Ref="J301"  Part="1" 
F 0 "J301" H 7050 1700 50  0000 C CNN
F 1 "Conn_01x05" H 7050 1100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 7050 1400 50  0001 C CNN
F 3 "" H 7050 1400 50  0001 C CNN
F 4 "Value" H 7050 1400 60  0001 C CNN "Fieldname"
	1    7050 1400
	1    0    0    -1  
$EndComp
$Comp
L jts-100-cartridge-adapter-rescue:Conn_01x06 J302
U 1 1 5AB23C7F
P 7050 3950
AR Path="/5AB23C7F" Ref="J302"  Part="1" 
AR Path="/5AB237B6/5AB23C7F" Ref="J302"  Part="1" 
F 0 "J302" H 7050 4250 50  0000 C CNN
F 1 "Conn_01x05" H 7050 3650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 7050 3950 50  0001 C CNN
F 3 "" H 7050 3950 50  0001 C CNN
F 4 "Value" H 7050 3950 60  0001 C CNN "Fieldname"
	1    7050 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2400 3150 1200
Wire Wire Line
	3150 1200 6850 1200
Wire Wire Line
	3650 2100 3650 1300
Wire Wire Line
	3650 1300 6850 1300
Wire Wire Line
	3800 2100 3800 1400
Wire Wire Line
	3800 1400 6850 1400
Wire Wire Line
	4000 2100 4000 1500
Wire Wire Line
	4000 1500 6850 1500
Wire Wire Line
	4150 2100 4150 1600
Wire Wire Line
	4150 1600 6850 1600
Wire Wire Line
	4650 2400 4650 1700
Wire Wire Line
	4650 1700 6850 1700
Wire Wire Line
	6850 3750 4650 3750
Wire Wire Line
	4650 3750 4650 3100
Wire Wire Line
	4150 3400 4150 3850
Wire Wire Line
	4150 3850 6850 3850
Wire Wire Line
	6850 3950 4000 3950
Wire Wire Line
	4000 3950 4000 3400
Wire Wire Line
	3800 3400 3800 4050
Wire Wire Line
	3800 4050 6850 4050
Wire Wire Line
	6850 4150 3650 4150
Wire Wire Line
	3650 4150 3650 3400
Wire Wire Line
	3150 3100 3150 4250
Wire Wire Line
	3150 4250 6850 4250
$EndSCHEMATC
